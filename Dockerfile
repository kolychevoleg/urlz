FROM tiangolo/uwsgi-nginx:python2.7-alpine3.7

COPY static /app/static
COPY templates /app/templates
COPY uwsgi.ini deps.txt main.py /app/
WORKDIR /app
RUN pip install virtualenv && \
    virtualenv --no-site-packages env && \
    source env/bin/activate && \
    pip install -r deps.txt

