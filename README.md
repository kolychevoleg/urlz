# Building and publishing docker image

`docker build -t kolychevoleg/urlz . && docker push kolychevoleg/urlz`

# Deploying to kubernetes cluster

`kubectl apply -f kubernetes.yml`
